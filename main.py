#!/usr/bin/python3

import asyncio
import configparser
import os
import sys
from logbook import Logger, StreamHandler
from coursebot import CourseBot
from nio import (
    AsyncClient,
    ClientConfig,
    InviteMemberEvent,
    RoomMessageText,
    SyncResponse,
)


async def message_cb(room, event):
    room_id = room.room_id
    bot = bots.get(room_id)
    if bot is None:
        if len(room.users.keys()) == 0:
            # Looks like the room is not fully in sync yet...
            await client.joined_members(room.room_id)
        bot = CourseBot(client, room)
        bots[room_id] = bot
    await bot.analyze(event)


async def sync_cb(response: SyncResponse):
    """ A callback that will be called every time our `sync_forever`
    method succesfully syncs with the server.
    """
    with open(next_batch, "w") as next_batch_token:
        next_batch_token.write(response.next_batch)


async def call_cb(room, event):
    await client.join(room.machine_name)


async def invited_cb(room, event):
    await client.join(room.room_id)


async def main():
    client.add_event_callback(message_cb, RoomMessageText)
    client.add_event_callback(invited_cb, InviteMemberEvent)
    client.add_response_callback(sync_cb, SyncResponse)
    with open(next_batch, "r+") as next_batch_token:
        client.next_batch = next_batch_token.read()
    await client.sync_forever(timeout=30000, full_state=True)


if __name__ == "__main__":
    cfg_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "coursebot.cfg"
    )
    if not os.path.isfile(cfg_file):
        print(f"config file {cfg_file} not found")
        sys.exit(1)
    config = configparser.ConfigParser()
    config.read(cfg_file)
    StreamHandler(sys.stdout).push_application()
    log = Logger("Logbook")
    host = config.get("user", "server")
    user = config.get("user", "username")
    bots = dict()
    client_config = ClientConfig(store_sync_tokens=True)
    client = AsyncClient(host, user, store_path=".", config=client_config)
    # Assign an access token to the bot instead of logging in and creating a new device
    client.access_token = config.get("user", "access_token")
    next_batch = os.path.join(os.path.dirname(__file__), "next_batch")
    asyncio.get_event_loop().run_until_complete(main())
