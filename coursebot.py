import os
from markdown import markdown
import time
import json


class CoureurListe(object):
    """ Gestion de la liste des coureurs"""

    def __init__(self, room):
        dossier_courant = os.path.dirname(os.path.realpath(__file__))
        self._chemin_sauvegarde = os.path.join(dossier_courant, room.room_id + ".json")
        if os.path.exists(self._chemin_sauvegarde):
            with open(self._chemin_sauvegarde) as f:
                self._coureurs = json.load(f)
        else:
            self._coureurs = dict()
            users = [room.user_name(user) for user in room.users.keys()]
            for user in users:
                if user == "InfoCourse":
                    continue
                self._coureurs[user] = "-"

    def obtenir(self):
        """Recuperer la liste de coureurs"""
        presents = []
        absents = []
        peutetres = []
        autres = []
        for nom, reponse in self._coureurs.items():
            if reponse == "ok":
                presents.append(nom)
            elif reponse == "no":
                absents.append(nom)
            elif reponse == "?":
                peutetres.append(nom)
            else:
                autres.append(nom)
        return (presents, absents, peutetres, autres)

    def ajouter(self, nom):
        """Ajout d'un coureur"""
        self._coureurs[nom] = "ok"
        self.sauvegarde()

    def enlever(self, nom):
        """Suppression d'un coureur"""
        self._coureurs[nom] = "no"
        self.sauvegarde()

    def peut_etre(self, nom):
        self._coureurs[nom] = "?"
        self.sauvegarde()

    def effacer(self):
        """Remise à zero des coureurs"""
        for nom in self._coureurs.keys():
            self._coureurs[nom] = "-"
        self.sauvegarde()

    def sauvegarde(self):
        """Sauvegarde la liste des coureurs dans un fichier"""
        with open(self._chemin_sauvegarde, "w") as fichier:
            json.dump(self._coureurs, fichier, indent=4)


class CourseBot(object):
    def __init__(self, nio_client, room):
        self._nio_client = nio_client
        self._room = room
        self.coureur_liste = CoureurListe(self._room)
        self._fonctions = {
            "!ok": self.ok,
            "!oui": self.ok,
            "!ko": self.ko,
            "!non": self.ko,
            "!aide": self.aide,
            "!info": self.aide,
            "!liste": self.liste,
            "!reset": self.reset,
            "!demande": self.demande,
            "!qvj": self.demande,
            "! ok": self.ok,
            "! oui": self.ok,
            "! ko": self.ko,
            "! non": self.ko,
            "! aide": self.aide,
            "! info": self.aide,
            "! liste": self.liste,
            "! reset": self.reset,
            "! demande": self.demande,
            "! qvj": self.demande,
            "!?": self.peutetre,
            "! ?": self.peutetre,
            "!pe": self.peutetre,
            "! pe": self.peutetre,
        }

    async def analyze(self, event):
        if event.sender == self._nio_client.user:
            # Ignorer messages du robot
            return
        message = event.body.lower()
        for commande, fonction in self._fonctions.items():
            if commande in message:
                await fonction(event)

    async def _send_notice(self, message):
        content = {
            "body": message,
            "formatted_body": markdown(message),
            "msgtype": "m.notice",
            "format": "org.matrix.custom.html",
        }
        await self._nio_client.room_send(
            self._room.machine_name, "m.room.message", content
        )

    def obtenir_nom_envoyeur(self, event):
        return self._room.user_name(event.sender)

    async def ok(self, event):
        coureur = self.obtenir_nom_envoyeur(event)
        self.coureur_liste.ajouter(coureur)
        await self._send_notice(f"_{coureur} a été ajouté à la liste._")

    async def peutetre(self, event):
        coureur = self.obtenir_nom_envoyeur(event)
        self.coureur_liste.peut_etre(coureur)
        await self._send_notice(f"_{coureur} n'est pas encore sûr._")

    async def ko(self, event):
        coureur = self.obtenir_nom_envoyeur(event)
        self.coureur_liste.enlever(coureur)
        await self._send_notice(f"_{coureur} a été enlevé de la liste._")

    async def liste(self, event):
        (presents, absents, peutetres, autres) = self.coureur_liste.obtenir()
        message = []
        message.append(
            f"** ✅ (x{len(presents)}), ❓️(x{len(peutetres)}), ❌(x{len(absents)}), 🙈 (x{len(autres)})** \n"
        )
        for present in presents:
            message.append(f"* ✅ {present}")
        for peutetre in peutetres:
            message.append(f"* ❓️ {peutetre}")
        for absent in absents:
            message.append(f"* ❌ {absent}")
        for autre in autres:
            message.append(f"* 🙈 {autre}")
        await self._send_notice(" \n".join(message))

    async def aide(self, event):
        await self._send_notice(
            """Utilitaire pour lister les coureurs. \n
Commandes disponibles :\n
- '!ok' ou '!oui: ajoute l'utilisateur courant dans la liste des coureurs.
- '!ko' ou '!non' : enlève l'utilisateur courant de la liste des coureurs.
- '!?' ou '!pe' : indique que l'utilisateur courant ne sait pas encore.
- '!liste' : donne le nbre et liste les coureurs.
- '!demande' ou '!qvj (Qui Vient Jeudi): réinitialise la liste et demande qui sera là la prochaine fois.
- '!aide' ou '!info' : affiche ce message.
            """
        )

    async def demande(self, event):
        # Nouvelle demande
        self.coureur_liste.effacer()
        demandeur = self.obtenir_nom_envoyeur(event)
        await self._send_notice(
            f"Qui vient courir jeudi chez _{demandeur}_ ? \n _(Répondez par '!oui' ou '!non' ou '!?')_"
        )
        coureur = self.obtenir_nom_envoyeur(event)
        self.coureur_liste.ajouter(coureur)

    async def reset(self, event):
        # reset liste
        self.coureur_liste.effacer()
        await self._send_notice("_La liste est éffacée._")
